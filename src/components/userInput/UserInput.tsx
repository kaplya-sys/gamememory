import React, {ChangeEvent, KeyboardEvent, useState} from "react"
import {useAppDispatch} from "../../app/hooks"
import {addUser} from "../../redux/redusers/userReduser"
import {readyUser} from "../../redux/redusers/gameReducer"

const UserInput = () => {
    const dispatch = useAppDispatch()
    const [userName, setUserName] = useState<string>('')

    const changeHandler = (): void => {
        dispatch(addUser(userName))
        dispatch(readyUser())
        setUserName('')
    }

    const handleKeypress = (event: KeyboardEvent<HTMLInputElement>) =>
    {
        event.key === 'Enter' && changeHandler()
    }

    return (
        <div className="col-md-12">
                <div className="input-group mb-3 px-2">
                    <input
                        type="text"
                        value={userName}
                        className="form-control"
                        placeholder="Представьтесь ..."
                        aria-label="Recipient's username"
                        aria-describedby="button-addon2"
                        onChange={(e: ChangeEvent<HTMLInputElement>):void => setUserName(e.target.value)}
                        onKeyPress={handleKeypress}
                    />
                    
                    <button
                        className="btn btn-success"
                        type="button"
                        id="button-addon2"
                        onClick={changeHandler}
                    >
                        Отправить
                    </button>
                </div>
        </div>
    )
}

export default UserInput
