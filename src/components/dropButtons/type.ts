export type IGameFieldVariables = Record<string, number>

export interface IDropButtonsProps {
    onClickGameField(mum: number):void
}