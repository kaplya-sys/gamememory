import React, {FC, MouseEvent} from "react"
import {gameFieldVariables} from "../../config/variables"
import {IDropButtonsProps, IGameFieldVariables} from "./type"


const DropButtons: FC<IDropButtonsProps> = ({onClickGameField}) => {
    const {heavy, normal, light}: IGameFieldVariables = gameFieldVariables

    return (
        <div className="col-md-auto">
            <div className="dropdown">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Выбор режима
                </button>

                <ul className="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <li>
                        <button
                            className="dropdown-item"
                            type="button"
                            onClick={(e: MouseEvent<HTMLButtonElement>):void => onClickGameField(light)}
                        >
                            Режим 2х2
                        </button>
                    </li>

                    <li>
                        <button
                            className="dropdown-item"
                            type="button"
                            onClick={(e: MouseEvent<HTMLButtonElement>):void => onClickGameField(normal)}
                        >
                            Режим 4х4
                        </button>
                    </li>
                    
                    <li>
                        <button
                            className="dropdown-item"
                            type="button"
                            onClick={(e: MouseEvent<HTMLButtonElement>):void => onClickGameField(heavy)}
                        >
                            Режим 6х6
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default DropButtons