import React, {FC, MouseEvent} from "react"
import {useAppSelector, useAppDispatch} from "../../app/hooks"
import {checkCards} from "../../redux/asyncThunk/checkCards"
import {ICardListProps} from "./type"

const CardsList: FC<ICardListProps> = ({value}) => {
    const dispatch = useAppDispatch()
    const {gameStart, cards, cardsDisabled} = useAppSelector(state => state.gameState)

    const selectHandler = (id: number): void => {
        if (gameStart && !cardsDisabled) {
            dispatch(checkCards(id))
        }
    }

    return (
        <div className="col-12 pt-3">
            <div className={value}>
                {cards.map(data => {
                    const {isGuessed, isOpen, cardName, id} = data                 // Вариант с удалением карт
                    return <img                                                    // {cards.filter(itm => !itm.isGuessed).map(data => {
                        src={isGuessed? '': isOpen?                                // const {isGuessed, isOpen, cardName, id} = data
                            `${process.env.PUBLIC_URL}/images/${cardName}`:        // return <img
                            `${process.env.PUBLIC_URL}/images/backend.png`         //     src={isOpen?
                        }                                                          //         `${process.env.PUBLIC_URL}/images/${cardName}`:
                        className="col-10"                                         //         `${process.env.PUBLIC_URL}/images/backend.png`}
                        alt=''
                        key={id}
                        onClick={(e: MouseEvent<HTMLImageElement>): void => selectHandler(id)}
                    />})
                }
            </div>
        </div>
    )
}

export default CardsList