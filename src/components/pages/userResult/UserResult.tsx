import React, {FC} from "react"
import {useAppSelector} from "../../../app/hooks"
import {msToTime} from "../../../utils/timeConverter"
import UserInput from "../../userInput/UserInput"

const UserResult: FC = () => {
    const {name, gameTime} = useAppSelector(state => state.userState.user)

    return (
        <div className="row">
            <UserInput />
            
            <div className="col-md-11 offset-1">
                <label className="fs-5 fst-italic">Игрок :</label>
                <div className="col-md-12 pt-2 text-primary">
                    {name}
                </div>
            </div>

            <div className="col-md-11 offset-1 pt-4">
                <label className="fs-5 fst-italic">Ваш результат :</label>
                <ul  className="col-md-12 pt-2">
                    {gameTime.map(data => <li key={data.time}>Режим {data.mode}x{data.mode}: {msToTime(data.time)}</li>)}
                </ul>
            </div>
        </div>
    )
}

export default UserResult