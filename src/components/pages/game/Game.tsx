import React, {FC, useEffect, useState} from "react"
import {useAppSelector, useAppDispatch} from "../../../app/hooks"
import {
    addCardList,
    endGame,
    startGame,
    readyGame,
    showAlert
} from "../../../redux/redusers/gameReducer"
import {gameMode, setTime} from "../../../redux/redusers/userReduser"
import {closeAlert} from "../../../redux/asyncThunk/closeAlert"
import {creatCardList} from "../../../utils/addCard"
import Timer from "../../timer/Timer"
import DropButtons from "../../dropButtons/DropButtons"
import CardsList from "../../cardsList/CardsList"
import StartButton from "../../startButton/StartButton"
import './game.css'

const Game: FC = () => {
    const dispatch = useAppDispatch()
    const {cards, counter} = useAppSelector(state => state.gameState)
    const {addUserName} = useAppSelector(state => state.gameState.gameReady)
    const [style, setStyle] = useState<string>('')

    useEffect((): void => {
        if (cards.length / 2 === counter && counter !== 0) {
            dispatch(endGame())
            dispatch(setTime(new Date().getTime()))
        }
    })

    const setGameField = (num: number): void => {
        dispatch(addCardList(creatCardList(num)))
        dispatch(readyGame())
        dispatch(gameMode(num))
        setStyle(`grid-container-${num}`)
    } 

    const startHandler = (): void => {
        if (addUserName) {
            dispatch(startGame())
        } else {
            dispatch(showAlert("Вы не ввели имя игрока!"))
            dispatch(closeAlert())
        }
    }

    return (
        <div className="row">
            <DropButtons onClickGameField={setGameField}/>

            <StartButton onClickStartGame={startHandler}/>

            <Timer />

            <CardsList value={style}/>
        </div>
    )
}

export default Game