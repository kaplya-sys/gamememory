import React, {FC} from "react"
import {useAppSelector} from "../../app/hooks"

const Alert: FC = () => {
    const text = useAppSelector(state => state.gameState.alert)

    return (
        <div className="alert alert-light alert-dismissible fade show fs-5 text-danger" role="alert">
            {text}
        </div>
    )
}

export default Alert