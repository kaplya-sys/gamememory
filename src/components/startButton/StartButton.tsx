import React, {FC} from "react"
import {useAppSelector} from "../../app/hooks"
import { IStartButtonProps } from "./type"

const StartButton: FC<IStartButtonProps> = ({onClickStartGame}) => {
    const {setDifficultGame} = useAppSelector(state => state.gameState.gameReady)

    return (
        <div className="col-md-3">
                <button
                    type='submit'
                    className='btn btn-success'
                    disabled={!setDifficultGame}
                    onClick={onClickStartGame}
                >
                    Старт
                </button>
        </div>
    )
}

export default StartButton