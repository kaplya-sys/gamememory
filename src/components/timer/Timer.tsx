import React, {useEffect, useState} from "react"
import {useAppSelector, useAppDispatch} from "../../app/hooks"
import {msToTime} from "../../utils/timeConverter"
import {setTime} from "../../redux/redusers/userReduser"

const Timer = () => {
    const gameStart = useAppSelector(state => state.gameState.gameStart)
    const dispatch = useAppDispatch()
    const [milliseconds, setMilliseconds] = useState<number>(0)

    useEffect(() => {
        let interval: any = null

         if (gameStart) {
            dispatch(setTime(new Date().getTime()))                     // Время запуска
            interval = setInterval(()=> {                               // Запуск таймера
                setMilliseconds(milliseconds => milliseconds + 1000)
            }, 1000)
        } else {
            clearInterval(interval)
            setMilliseconds(0)
        }
        return () => clearInterval(interval)
    }, [gameStart, dispatch])

    return (
        <div className="col-md-auto fs-2">
            {msToTime(milliseconds)}
        </div>
    )
}

export default Timer