import React, {FC} from "react"
import Game from "./components/pages/game/Game"
import UserResult from "./components/pages/userResult/UserResult"
import Alert from "./components/alert/Alert"

const App: FC = () => {

  return (
    <div className="container-fluid pt-3">
      <div className="row">
          <div className="col-md-3">
              <UserResult/>
          </div>

          <div className="col-md-2">
              <Alert/>
          </div>
          
          <div className="col-md-4">
              <Game/>
          </div>
      </div>
    </div>
  )
}

export default App
