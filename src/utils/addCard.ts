import {ICardsType} from "../redux/redusers/type"

export const creatCardList = (size: number): ICardsType[] => {
    const cards: ICardsType[] = []

    const shuffle = (array: ICardsType[]): ICardsType[] => {  //алгоритм Фишера-Йетса.
        let j: number, temp: ICardsType
        for(let i: number = array.length - 1; i > 0; i--){
            j = Math.floor(Math.random()*(i + 1))
            temp = array[j]
            array[j] = array[i]
            array[i] = temp
        }
        return array
    }

    let counter: number = 0
    
    for (let i: number = 0; i < Math.pow(size, 2); i++) {        // Создаем массив карты, в зависимости от
        cards.push(                                              // выбранной сложности 2х2, 4х4, 6х6
            {
                id: i,
                isOpen: false,
                isGuessed: false,
                cardName: `${counter}.png`
            }
        )
        if (counter === Math.pow(size, 2) / 2 - 1) {
            counter = 0
        } else {
            counter += 1
        }
    }
    return shuffle(cards)
}