import {createAsyncThunk} from "@reduxjs/toolkit"
import {RootState, AppDispatch} from "../store/store"
import {
    closeCard,
    guessedCard,
    lockedCards,
    openCard,
    selectedCard
} from "../redusers/gameReducer"

export const checkCards = createAsyncThunk<unknown, number, {
    state: RootState
    dispatch: AppDispatch }>(
    'gameSlice/checkCards',
    (cardId, {getState, dispatch}) => {
        const {cards, firstCard} = getState().gameState
        const newItem = cards.find(item => item.id === cardId && !item.isOpen)

        if (newItem) {
            const {id, cardName} = newItem
            dispatch(openCard(id))

            if (!firstCard) {
                dispatch(selectedCard(cardName))
            } else {
                const clear = () => clearTimeout(timer)
                dispatch(lockedCards())
                const timer = setTimeout(() => {
                    
                    if (firstCard === cardName) {
                        dispatch(guessedCard())
                    } else {
                        dispatch(closeCard(cardName))
                    }
                    dispatch(selectedCard(null))
                    clear()
                }, 1000)
            }
        }
    }
)