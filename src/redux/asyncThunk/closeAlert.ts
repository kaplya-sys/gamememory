import {createAsyncThunk} from "@reduxjs/toolkit"
import {AppDispatch} from "../store/store"
import {hideAlert} from "../redusers/gameReducer"

export const closeAlert = createAsyncThunk<unknown, void, {dispatch: AppDispatch}>(
    'gameSlice/closeAlert',
    (_, {dispatch}) => {
        const clear = () => clearTimeout(timer)
        const timer = setTimeout(() => {
            dispatch(hideAlert())
            clear()
        }, 2000)
    }
)