import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {IUserReducerType} from "./type"

const initialState: IUserReducerType = {
    time: null,
    gameMode: null,
    user: {
        name: null,
        gameTime: []
    }
}

export const userReducer = createSlice({
    name: 'userSlice',
    initialState,
    reducers: {
        setTime: (state: IUserReducerType, action: PayloadAction<number>) => {
            if (state.time) {
                const result: number = action.payload - state.time
                const gameResult = {mode: state.gameMode, time: result}

                state.user.gameTime = state.user.gameTime.concat(gameResult)
                state.time = null
            } else {
                state.time = action.payload
            }
        },
        addUser: (state: IUserReducerType, action: PayloadAction<string>) => {
            state.user.name = action.payload
            state.user.gameTime = []
        },
        gameMode: (state: IUserReducerType, action: PayloadAction<number>) => {
            state.gameMode = action.payload
        }
    }
})

export const {setTime ,addUser, gameMode} = userReducer.actions