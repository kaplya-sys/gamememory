import {combineReducers} from "@reduxjs/toolkit"
import {gameReducer} from "./gameReducer"
import {userReducer} from "./userReduser"

export const rootReducer = combineReducers({
    userState: userReducer.reducer,
    gameState: gameReducer.reducer
})