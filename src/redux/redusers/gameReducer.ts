import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {IGameReducerType, ICardsType} from "./type"

const initialState: IGameReducerType = {
    firstCard: null,
    cards: [],
    gameStart: false,
    counter: 0,
    cardsDisabled: false,
    alert: null,
    gameReady: {
        addUserName: false,
        setDifficultGame: false
    }
}

export const gameReducer = createSlice({
    name: 'gameSlice',
    initialState,
    reducers: {
        startGame: (state: IGameReducerType) => {
            state.gameStart = true
        },
        endGame: (state: IGameReducerType) => {
            state.gameStart = false
            state.counter = 0
            state.gameReady.setDifficultGame = false
            
        },
        addCardList: (state: IGameReducerType, action: PayloadAction<ICardsType[]>) => {
            state.cards = action.payload
            state.gameStart = false
        },
        addUser: (state: IGameReducerType) => {
            state.gameStart = false
        },
        showAlert: (state: IGameReducerType, action: PayloadAction<string>) => {
            state.alert = action.payload
        },
        hideAlert: (state: IGameReducerType) => {
            state.alert = null
        },
        readyUser: (state: IGameReducerType) => {
            state.gameReady.addUserName = true
        },
        readyGame: (state: IGameReducerType) => {
            state.gameReady.setDifficultGame = true
        },
        openCard: (state: IGameReducerType, action: PayloadAction<number>) => {
            state.cards = state.cards.map(
                item => item.id === action.payload ?
                    {...item, isOpen: true} :
                    item
            )
        },
        closeCard: (state: IGameReducerType, action: PayloadAction<string>) => {
            state.cards = state.cards.map(
                item => item.cardName === action.payload || item.cardName === state.firstCard ?
                    {...item, isOpen: false} :
                    item
            )
            state.cardsDisabled = false
        },
        guessedCard: (state: IGameReducerType) => {
            state.cards = state.cards.map(
                item => item.cardName === state.firstCard ?
                    {...item, isOpen: false, isGuessed: true} :
                    item
            )
            state.counter += 1
            state.cardsDisabled = false
        },
        selectedCard: (state: IGameReducerType, action: PayloadAction<string | null>) => {
            state.firstCard = action.payload
        },
        lockedCards: (state: IGameReducerType) => {
            state.cardsDisabled = true
        }
    }
})

export const {
    startGame,
    endGame,
    addCardList,
    openCard,
    closeCard,
    guessedCard,
    selectedCard,
    lockedCards,
    showAlert,
    hideAlert,
    readyUser,
    readyGame
} = gameReducer.actions