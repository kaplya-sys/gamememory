export interface IGameReducerType {
    firstCard: string | null
    cards: ICardsType[]
    gameStart: boolean
    counter: number
    cardsDisabled: boolean
    alert: string | null
    gameReady: {
        addUserName: boolean
        setDifficultGame: boolean
    }
}


export interface IUserReducerType {
    time: number | null
    gameMode: number | null
    user: {
        name: string | null
        gameTime: any[]
    }
}

export interface ICardsType {
    id: number
    isOpen: boolean
    isGuessed: boolean
    cardName: string
}